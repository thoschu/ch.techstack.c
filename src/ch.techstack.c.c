/*
 ============================================================================
 Name        : c.c
 Author      : Tom S,
 Version     :
 Copyright   : MIT
 Description : Hello World in C, Ansi-style
 ============================================================================
 */

#include <stdio.h>
#include <stdlib.h>
#include <limits.h>
#include <float.h>

#define GETHEADER(x) printf("# %s Programmiersprache\n", #x)
#define BY "by Tom S."

int main(void) {
	puts("Hello World in C " BY);  // prints Hello World in C by Tom S.
	#undef BY
	puts("");

	GETHEADER("H�here");
	puts(" > Menschenlesbarer Programmiercode");
	puts("");

	GETHEADER("Imperative");
	puts(" > Folge von Anweisungen im Programmspeicher");
	puts("");

	GETHEADER("Prozedurale");
	puts(" > Algorithmen in �berschaubare Teile zerlegen");
	puts("");

	puts("----------------------------------------------");
	puts("");

	/* Variablen und Zahlen */

	// int = Ganze Zahlen
	signed int number1; 		// Deklaration
	number1 = 13; 				// Initialisierung

	int number2 = number1 - 20;	// Deklaration + Initialisierung

	printf("%d\n", number2);

	// unsigned int = Nat�rliche Zahlen
	unsigned int number3 = 77;

	printf("%d\n", number3);

	// float und double = "Reelle Zahlen" / "Rationale Zahlen"
	float number4 = 3.25;
	float number5 = number4 * (-1);

	printf("%f\n", number5);

	double number6 = 0.0;
	number6 = number5;
	number6 = number6 + 1; 		// Addition
	number6 = number6 - 1; 		// Subtraktion
	number6 *= -1000; 			// Multiplikation per abk�rzende Rechenoperation = number6 = number6 * -1000
	number6 /= 10; 				// Division per abk�rzende Rechenoperation = number6 = number6 / 10

	printf("%lf\n", number6);
	printf("%.2lf\n", number6);
	puts("");

//	char string[] = "Bitte Zahlen eingeben: \n";
//	printf(string);
//
//	float number7;
//	int number8;
//
//	scanf("%f", &number7);
//	scanf("%d", &number8);
//
//	double number9 = number7 * number8;
//
//	printf("Ergebnis: %lf\n", number9);
//	puts("");

	puts("http://www.tutorialspoint.com/cprogramming/c_data_types.htm");
	puts("");

	float number10 = 1.3;
	printf("float - number10: %f can be written %e\n", number10, number10);
	printf("Size of float: %d\n", sizeof(float));
	printf("Size of number10: %d\n", sizeof(number10));
    printf("The maximum value of float = %.10E\n", FLT_MAX);
	printf("The minimum value of float = %.10E\n", FLT_MIN);
	puts("");

	double number11 = 7.7777777777777777;
	printf("double - number11: %f can be written %e\n", number11, number11);
	printf("Size of double: %d\n", sizeof(double));
	printf("Size of number11: %d\n", sizeof(number11));
    printf("The maximum value of double = %f\n", DBL_MAX);
	printf("The minimum value of double = %f\n", DBL_MIN);
	puts("");

	long double number12 = 13.77;
//	printf("long double - number12: %Lf\n", number12);
	printf("Size of long double: %d\n", sizeof(long double));
	printf("Size of number12: %d\n", sizeof(number12));
//    printf("The maximum value of long double = %e\n", LDBL_MAX);
//	printf("The minimum value of long double = %e\n", LDBL_MIN);
//	puts("");

	int number13 = 77;
	printf("int - number13: %d\n", number13);
	printf("Size of int: %d\n", sizeof(int));
	printf("Size of number13: %d\n", sizeof(number13));
    printf("The maximum value of int = %d\n", INT_MAX);
	printf("The minimum value of int = %d\n", INT_MIN);
	puts("");

	signed int number14 = -77;
	printf("signed int - number14: %d\n", number14);
	printf("Size of signed int: %d\n", sizeof(signed int));
	printf("Size of number14: %d\n", sizeof(number14));
    printf("The maximum value of signed int = %d\n", INT_MAX);
	printf("The minimum value of signed int = %d\n", INT_MIN);
	puts("");

	unsigned int number15 = 1977;
	printf("unsigned int - number15: %d\n", number15);
	printf("Size of unsigned int: %d\n", sizeof(unsigned int));
	printf("Size of number15: %d\n", sizeof(number15));
    printf("The maximum value of unsigned int = %d\n", UINT_MAX);
	// printf("The minimum value of unsigned int = %d\n", UINT_MAX);
	puts("");

	char number16 = 16;
	printf("char - number16: %d\n", number16);
	printf("Size of char: %d\n", sizeof(char));
	printf("Size of number16: %d\n", sizeof(number16));
    printf("The maximum value of char = %d\n", CHAR_MAX);
	printf("The minimum value of char = %d\n", CHAR_MIN);
	printf("CHAR_BIT: %d", CHAR_BIT);
	puts("");

	unsigned char number17 = 62;
	printf("unsigned char - number17: %d\n", number17);
	printf("Size of unsigned char: %d\n", sizeof(unsigned char));
	printf("Size of number17: %d\n", sizeof(number17));
    printf("The maximum value of unsigned char = %d\n", UCHAR_MAX);
	// printf("The minimum value of unsigned char = %d\n", UCHAR_MAX);
	puts("");

	short number18 = 100;
	printf("short - number18: %d\n", number18);
	printf("Size of short: %d\n", sizeof(short));
	printf("Size of number18: %d\n", sizeof(number18));
    printf("The maximum value of short = %d\n", SHRT_MAX);
	printf("The minimum value of short = %d\n", SHRT_MIN);
	puts("");

	signed short number19 = 200;
	printf("signed short - number19: %d\n", number19);
	printf("Size of signed short: %d\n", sizeof(signed short));
	printf("Size of number19: %d\n", sizeof(number19));
    printf("The maximum value of signed short = %d\n", SHRT_MAX);
	printf("The minimum value of signed short = %d\n", SHRT_MIN);
	puts("");

	unsigned short number20 = 400;
	printf("unsigned short - number20: %d\n", number20);
	printf("Size of unsigned short: %d\n", sizeof(unsigned short));
	printf("Size of number20: %d\n", sizeof(number20));
    printf("The maximum value of unsigned short = %d\n", USHRT_MAX);
	// printf("The minimum value of signed short = %d\n", USHRT_MAX);
	puts("");

    long number21 = 700;
//	printf("long - number21: %Ld\n", number21);
	printf("Size of long: %d\n", sizeof(long));
	printf("Size of number21: %d\n", sizeof(number21));
//  printf("The maximum value of long = %Ld\n", LONG_MAX);
//	printf("The minimum value of long = %Ld\n", LONG_MIN);
	puts("");

	puts("https://www.ascii.cl");
	puts("");

	char value1 = 65;
	printf("char - value1: %c\n", value1);
	printf("char - value1: %d\n", value1);
	printf("char - value1: %x\n", value1);
	puts("");

	char value2 = '@';
	printf("char - value2: %c\n", value2);
	printf("char - value2: %d\n", value2);
	printf("char - value2: %x\n", value2);
	puts("");

	char value3 = '\x4a';
	printf("char - value3: %c\n", value3);
	printf("char - value3: %d\n", value3);
	printf("char - value3: %x\n", value3);
	puts("");

	char value4 = 0x5a;
	printf("char - value3: %c\n", value4);
	printf("char - value3: %d\n", value4);
	printf("char - value3: %x\n", value4);
	puts("");

	char value5[] = "TOM";
	printf("char - value5: %s\n", value5);
	printf("char - value5 at index 0: %c\n", value5[0]);
	printf("char - value5 at index 1: %d\n", value5[1]);
	printf("char - value5 at index 2: %x\n", value5[2]);
	puts("");

	int value6 = 312;			// dezimal
	int value7 = 0b100111000;	// bin�r
	int value8 = 0x138;			// hexadezimal
	int value9 = 0470;			// oktal
	printf("int - value6: %d\n", value6);
	printf("int - value6: %o\n", value6);
	printf("int - value7: %d\n", value7);
	printf("int - value8: %d\n", value8);
	printf("int - value9: %d\n", value9);
	puts("");

	return EXIT_SUCCESS;
}
